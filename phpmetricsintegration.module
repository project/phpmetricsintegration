<?php

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function phpmetricsintegration_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.phpmetricsintegration':
      return t('
        <h2>PHPMetrics analysis tool for Drupal</h2>
        <hr>
        <p>PHPMetrics is a Static Analysis tool for PHP.</p>
        <p>PHPMetrics documentation is available <a href="http://www.phpmetrics.org/documentation/index.html">here</a>.</p>
        <h3>Module Requirement</h3>
        <p>Requires <code>phpmetrics.phar</code> to be present at <kbd>/vendor/bin</kbd> directory of this module. For ease of use, a phar file is already present at the desired location to make the installation and use simpler. However if you intend to use a different version, you can do so by downloading the phar and replacing the default file. Checkout PHPMetrics phar releases <a href="https://github.com/phpmetrics/PhpMetrics/releases">here</a>.</p>
        <h3>Configuration Summary</h3>
        <p>PHPMetrics can scan and generate report for a directory. We recomend using it on your custom modules directory to validate the custom code running in your site.</p>
        <p>To change the scan directory and report generation directory, goto <a href="/admin/config/development/phpmetricsintegration/configuration">the configuration page</a>.</p>
        <h3>Additional Information</h3>
        <p>You can run <code>composer install -o</code> to install the dependencies in the module directory, although it is highly recomended to use official phar releases. Use the composer.json if you want to customise/tweak for some custom magic!</p>
        <hr>
        <h3>Issues/Todo:</h3>
        <ul>
          <li>Unit tests will be published with next version</li>
          <li>CRON job to delete logs</li>
          <li>Theme log messages (planned)</li>
          <li>Default configurations to be streamlined</li>
        </ul>
        <hr>
        <h3>Authors:</h3>
        <ul>
          <li>Aniruddha Banerjee  <a href="mailto:aniruddha.banerjee@tcs.com">aniruddha.banerjee@tcs.com</a></li>
          <li>Ramit Mitra  <a href="mailto:ramit.mitra@gmail.com">ramit.mitra@gmail.com</a></li>
        </ul>
      ');
  }
}

/**
 * Implements hook_theme().
 */
function phpmetricsintegration_theme($existing, $type, $theme, $path) {
  return [
    'success_report' => [
      'variables' => [
        'logs' => NULL
      ],
    ],
  ];
}