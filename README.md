# PHPMetrics Analysis  

## About PHPMetrics   
PhpMetrics provides metrics about PHP project and classes, with beautiful and readable HTML report.   
[Learn more about PHPMetrics.](http://www.phpmetrics.org/)    

## About PHPMetrics Analysis   
This PHPMetrics Ananlysis module for Drupal brings PHPMetrics analysis to your drupal site. Run analysis and get insights on the quality of your code. Insights help you produce better code and take care of possible issues before they occur, thereby enhancing the stability of your site.   

## Credits
1. PHPMetrics for building this awesome SCA tool, and available at https://github.com/phpmetrics/PhpMetrics

## Contact
1. Ramit Mitra <ramit.mitra@tcs.com>
2. Aniruddha Banerjee <aniruddha.banerjee@tcs.com>